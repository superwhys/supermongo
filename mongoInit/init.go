package mongoInit

import (
	"gitee.com/superwhys/supermongo/mongoCRUD"
	"gitee.com/superwhys/supermongo/mongoConnect"
	"go.mongodb.org/mongo-driver/mongo"
)

type Database struct {
	Mongo *mongo.Client
}

var DB *Database
var MC *mongoCRUD.MCollection

func Init(mongoUrl, database, collection string) (*Database, *mongoCRUD.MCollection) {
	DB = &Database{
		Mongo: mongoConnect.ConnectMongo(mongoUrl),
	}
	MC = &mongoCRUD.MCollection{
		Collection: DB.NewCollect(database, collection),
	}
	return DB, MC
}

func (m *Database) NewCollect(database, collection string) *mongo.Collection {
	coll := m.Mongo.Database(database).Collection(collection)
	return coll
}
