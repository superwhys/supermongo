package mongoCRUD

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

type MCollection struct {
	Collection *mongo.Collection
}


func (mc *MCollection) MongoInsert(document interface{}) *mongo.InsertOneResult {
	insertRes, err := mc.Collection.InsertOne(context.TODO(), document)
	if err != nil {
		log.Fatalln(err)
	}
	return insertRes
}

func (mc *MCollection) MongoInsertMany(documents []interface{}) *mongo.InsertManyResult {
	insertManyRes, err := mc.Collection.InsertMany(context.TODO(), documents)
	if err != nil {
		log.Fatalln(err)
	}
	return insertManyRes
}

func (mc *MCollection) MongoFind(filter interface{}, result interface{}) *mongo.SingleResult {
	SingleResult := mc.Collection.FindOne(context.TODO(), filter)
	err := SingleResult.Decode(result)
	if err != nil {
		log.Fatalln(err)
	}
	return SingleResult
}

func (mc *MCollection) MongoFindMany(filter interface{}, opts ...*options.FindOptions) *mongo.Cursor {
	var cur *mongo.Cursor
	var err error

	if len(opts) != 0 {
		cur, err = mc.Collection.Find(context.TODO(), filter, opts[0])
	} else{
		cur, err = mc.Collection.Find(context.TODO(), filter)
	}
	if err != nil {
		log.Fatalln(err)
	}
	return cur
}

func (mc *MCollection) MongoUpdate(filter interface{}, update interface{},
	opts ...*options.UpdateOptions) *mongo.UpdateResult {

	var updateRes *mongo.UpdateResult
	var err error
	fmt.Println(len(opts))
	if len(opts) != 0 {
		updateRes, err = mc.Collection.UpdateOne(context.TODO(),
			filter, update, opts[0])
	} else {
		updateRes, err = mc.Collection.UpdateOne(context.TODO(), filter, update)

	}
	if err != nil {
		log.Fatalln(err)
	}

	return updateRes
}
