package mongoConnect

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

func ConnectMongo(mongoUrl string) *mongo.Client {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	clientOptions := options.Client().ApplyURI(mongoUrl)

	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		log.Fatalln(err)
	}

	err = client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Connect to mongodb")
	return client
}
