package main

import (
	"context"
	"fmt"
	"gitee/superwhys/supermongo/mongoInit"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

type person struct {
	Name string
	Age int
}


func main() {
	mongoUrl := "mongodb://localhost:27017"
	_, collection := mongoInit.Init(mongoUrl, "test", "person")

	// Insert
	//collection.MongoInsert(struct {
	//	Name string
	//	Age int
	//}{"superYong", 18})

	//filter := bson.D{
	//	{"name", "superYong"},
	//}

	var result person
	// FindOne
	//collection.MongoFind(filter, &result)
	//fmt.Printf("%v\n", &result)

	// FindMany
	findOptions := options.Find()
	findOptions.SetLimit(2)
	cur := collection.MongoFindMany(bson.D{{}}, findOptions)
	fmt.Println(cur)

	for cur.Next(context.TODO()) {
		err := cur.Decode(&result)
		if err != nil {
			log.Fatalln(err)
		}
		fmt.Println(&result)
	}

	// Update
	updateFilter := bson.D{
		{"name", "superYong"},
	}
	update := bson.D{
		{"$set", bson.D{
			{"name", "aaa"},
			{"age", 50},
		}},
	}

	updateOptions := options.Update()
	updateOptions.SetUpsert(true)
	updateRes := collection.MongoUpdate(updateFilter, update, updateOptions)
	fmt.Println(updateRes)
}
